﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float Speed;

    private Vector2 currentPosition;
    public bool isTouchingLeft, isTouchingUp, isTouchingRight, isTouchingDown;

    private void Start()
    {
        isTouchingLeft = false;
        isTouchingUp = false;
        isTouchingRight = false;
        isTouchingDown = false;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow) && !isTouchingLeft)
            currentPosition.x -= Speed * Time.deltaTime;
        if (Input.GetKey(KeyCode.RightArrow) && !isTouchingRight)
            currentPosition.x += Speed * Time.deltaTime;
        if (Input.GetKey(KeyCode.UpArrow) && !isTouchingUp)
            currentPosition.y += Speed * Time.deltaTime;
        if (Input.GetKey(KeyCode.DownArrow) && !isTouchingDown)
            currentPosition.y -= Speed * Time.deltaTime;

        transform.position = currentPosition;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name.Equals("Wall_Left"))
            isTouchingLeft = true;
        if (collision.name.Equals("Wall_Right"))
            isTouchingRight = true;
        if (collision.name.Equals("Wall_Top"))
            isTouchingUp = true;
        if (collision.name.Equals("Wall_Bottom"))
            isTouchingDown = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.name.Equals("Wall_Left"))
            isTouchingLeft = false;
        if (collision.name.Equals("Wall_Right"))
            isTouchingRight = false;
        if (collision.name.Equals("Wall_Top"))
            isTouchingUp = false;
        if (collision.name.Equals("Wall_Bottom"))
            isTouchingDown = false;
    }
}